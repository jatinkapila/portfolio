var display = ""; // String format to feed into.

var counter = 6;
var state = 0; // 0 => no entry.
// 1 => first digit entered.
// ...
// 4 => fourth digit is entered.
// if state > 4 don't accept any further inputs unless clear is hit.
var timer;
var blinkTimer;

// Updates timer on the clock.

document.getElementById('timerDisplay').style.font = "30px digital"

function timerButtonClick(num) {

  if (state < 4) {
    display = display + num;
    state++;
  }
  document.getElementById('timerDisplay').innerHTML = getClockNotation();
}

// return's time in "nn:nn" format
function getClockNotation() {

  var ans = "";
  ans += (display.length >= 4) ? display.substring(0,1) : "";
  ans += (display.length >= 3) ? display.slice(-3).substring(0,1) : "0";
  ans +=  ":";
  ans += (display.length >= 2) ? display.slice(-2).substring(0,1) : "0";
  ans += (display.length >= 1) ? display.slice(-1)  : "0";

  return ans;
}



// I put an "_" because apparently start, clear and cancel are in-built JS functions and I wasn't able to invoke them in html.

function _start(){

  if (display == "") return;
  document.getElementById('timerDisplay').innerHTML= getClockNotation();

  if (parseInt(display) % 100 == 0) {
    display = "" + ((parseInt(display)) - 40);
  }

  display = "" + (parseInt(display) - 1);
  if(parseInt(display)<0){
    clearTimeout(display);
    document.getElementById('glass').style.backgroundColor = "black";

    // document.getElementById('timerDisplay').innerHTML = "";
    document.getElementById('timerDisplay').innerHTML = "Done";
    counter = 6;
    changeScreenColor("white");

    state = 0;
    display = "";
    return;


  }
  document.getElementById('glass').style.backgroundColor = "white";

  timer = setTimeout("_start()", 1000)

}



function changeScreenColor(color) {
  if (counter <= 0) {
    clearTimeout(blinkTimer);
    return;
  }
  document.getElementById('timerScreen').style.backgroundColor = color;
  counter--;
  if (color == "white") {
    document.getElementById('timerDisplay').style.color = "black";
    blinkTimer = setTimeout("changeScreenColor('black')", 1000);
  } else {
    document.getElementById('timerDisplay').style.color = "white";
    blinkTimer = setTimeout("changeScreenColor('white')", 1000);
  }
}


function _clear() {
  document.getElementById('glass').style.backgroundColor = "black";
  document.getElementById('timerScreen').style.backgroundColor = "black";
  document.getElementById('timerDisplay').style.color = "white";
  clearTimeout(timer);
  clearTimeout(blinkTimer);
  state = 0;
  display = "";
  document.getElementById('timerDisplay').innerHTML = "&nbsp;";
}

// the stop button function
function _cancel(){

  clearTimeout(timer);

  document.getElementById('glass').style.backgroundColor = "black";
  document.getElementById('timerDisplay').innerHTML = "STOPPED";
}
